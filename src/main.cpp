//
// main.cpp for main in /home/kerdil_r/rendu/Another-IRC-server
// 
// Made by Kerdiles Robin
// Login   <robin.kerdiles@epitech.eu>
// 
// Started on  Thu Aug 27 01:43:09 2015 Kerdiles Robin
// Last update Fri Aug 28 03:04:07 2015 Kerdiles Robin
//

#include				<boost/asio.hpp>
#include				"Server.h"

int					main(int ac, char **av)
{
  boost::asio::io_service		io_service;
  boost::asio::ip::tcp::endpoint	endpoint(boost::asio::ip::tcp::v4(),
						 4242);
  Server				server(io_service, endpoint);
  (void)ac;
  (void)av;
  return (0);
}
