//
// Server.cpp for Server in /home/kerdil_r/rendu/Another-IRC-server
// 
// Made by Kerdiles Robin
// Login   <robin.kerdiles@epitech.eu>
// 
// Started on  Fri Aug 28 00:19:58 2015 Kerdiles Robin
// Last update Fri Aug 28 02:54:44 2015 Kerdiles Robin
//

#include	"Server.h"

Server::Server(boost::asio::io_service& io_service,
	       const boost::asio::ip::tcp::endpoint& endpoint)
  : io_service_(io_service), endpoint_(endpoint),
    acceptor_(io_service, endpoint)
{
}

Server::~Server()
{
}
