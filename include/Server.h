/*
** Server.h for Server in /home/kerdil_r/rendu/Another-IRC-server
** 
** Made by Kerdiles Robin
** Login   <robin.kerdiles@epitech.eu>
** 
** Started on  Fri Aug 28 00:15:10 2015 Kerdiles Robin
// Last update Fri Aug 28 02:52:33 2015 Kerdiles Robin
*/

#ifndef SERVER_H_
# define SERVER_H_

#include				<boost/asio.hpp>

class Server
{
public:
  /* Allowed ctor */
  Server(boost::asio::io_service& io_service,
	 const boost::asio::ip::tcp::endpoint& endpoint);
  virtual				~Server();
private:
  /* Attributes */
  boost::asio::io_service&		io_service_;
  const boost::asio::ip::tcp::endpoint& endpoint_;
  boost::asio::ip::tcp::acceptor	acceptor_;
};

#endif /* !SERVER_H_ */
