#! /bin/sh

git submodule init
git submodule sync
git submodule update

test -d m4 || mkdir -v m4

cp boost.m4/build-aux/boost.m4 m4

aclocal	\
&& automake --add-missing \
&& autoconf
